export class FizzBuzz {
    getString(number) {
        if(this.isFizzBuzz(number)){
            return "FizzBuzz"
        }
        
        if(number%5 != 0 && number%3 != 0){
            return number.toString()
        }
        if(number%5 == 0){
            return "Buzz"
        }
        
        return "Fizz"
    }
    isFizzBuzz(number){
        return number%5 == 0 && number%3 == 0
    }
}



