import { FizzBuzz } from '../src/FizzBuzz.js'

describe('El FizzBuzz', () => {
    it('tiene que escribir cada número no divisible entre 3 o 5', () => {
        // Arrange
        const expected_value = "1"

        // Act
        const result = FizzBuzz(1);

        // Assert
        expect(result).toBe(expected_value)
    })

    it('tiene que decir Fizz con cada número divisible entre 3', () => {
        // Arrange
        const expected_value = "1 2 Fizz"

        // Act
        const result = FizzBuzz(3);

        // Assert
        expect(result).toBe(expected_value)
    })

    it('tiene que decir Buzz con cada número divisible entre 5', () => {
        // Arrange
        const expected_value = "1 2 Fizz 4 Buzz"

        // Act
        const result = FizzBuzz(5);

        // Assert
        expect(result).toBe(expected_value)
    })

    it('tiene que decir FizzBuzz con cada número divisible entre 3 y 5', () => {
        // Arrange
        const expected_value = "1 2 Fizz 4 Buzz Fizz 7 8 Fizz Buzz 11 Fizz 13 14 FizzBuzz"

        // Act
        const result = FizzBuzz(15);

        // Assert
        expect(result).toBe(expected_value)
    })

    it('tiene que hacer el proceso hasta el numero 100', () => {
        // Arrange
        const expected_value = "1 2 Fizz 4 Buzz Fizz 7 8 Fizz Buzz 11 Fizz 13 14 FizzBuzz 16 17 Fizz 19 Buzz Fizz 22 23 Fizz Buzz 26 Fizz 28 29 FizzBuzz 31 32 Fizz 34 Buzz Fizz 37 38 Fizz Buzz 41 Fizz 43 44 FizzBuzz 46 47 Fizz 49 Buzz Fizz 52 53 Fizz Buzz 56 Fizz 58 59 FizzBuzz 61 62 Fizz 64 Buzz Fizz 67 68 Fizz Buzz 71 Fizz 73 74 FizzBuzz 76 77 Fizz 79 Buzz Fizz 82 83 Fizz Buzz 86 Fizz 88 89 FizzBuzz 91 92 Fizz 94 Buzz Fizz 97 98 Fizz Buzz"

        // Act
        const result = FizzBuzz();

        // Assert
        expect(result).toBe(expected_value)
    })

    
})
