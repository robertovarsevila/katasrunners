import { mars_rover } from '../src/mars_rover.js'   

describe('The Mars Rover ', () => {
    it('knows the location of its landing zone', () => {
        // Arrange
        const expected_value    = '5,5,N'
        const landingLoc        = '5,5,N'
        // Act
        const result = mars_rover(landingLoc)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('Moves forward when recieving an M command', () => {
        // Arrange
        const expected_value    = '1,5,N'
        const landingLoc        = '1,2,N'
        const command           = 'MMM'
        // Act
        const result = mars_rover(landingLoc, command)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('can change direction to the right', () => {
        // Arrange
        const expected_value    = '3,1,E'
        const landingLoc        = '1,1,N'
        const command           = 'RMM'
        // Act
        const result = mars_rover(landingLoc, command)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('can change direction to the left', () => {
        // Arrange
        const expected_value    = '999,1,W'
        const landingLoc        = '1,1,N'
        const command           = 'LMM'
        // Act
        const result = mars_rover(landingLoc, command)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('can go south', () => {
        // Arrange
        const expected_value    = '1,999,S'
        const landingLoc        = '1,1,N'
        const command           = 'LLM'
        // Act
        const result = mars_rover(landingLoc, command)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('can turn around and not get sick', () => {
        // Arrange
        const expected_value    = '1,1,N'
        const landingLoc        = '1,1,N'
        const command           = 'LLLLRRRR'
        // Act
        const result = mars_rover(landingLoc, command)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('knows the planet size', () => {
        // Arrange
        const expected_value    = '1,1,N'
        const landingLoc        = '1,1,N'
        const command           = 'MMMRMMML'
        const planetSize        = '3,3'
        // Act
        const result = mars_rover(landingLoc, command, planetSize)

        // Assert
        expect(result).toBe(expected_value)
    })
})
