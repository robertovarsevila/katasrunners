export class MarsRover {
  constructor() {
    this.x = 0
    this.y = 0
    this.facing = 0
    this.directions = ['North', 'East', 'South', 'West']
    this.displacementVector = [
      [0,1],
      [1,0],
      [0,-1],
      [-1,0]
    ]
    this.orders = {
      M: this.move.bind(this),
      R: this.turnRight.bind(this),
      L: this.turnLeft.bind(this),
    }
  }

  isInPosition(x, y) {
    return this.x === x && this.y === y
  }

  giveOrder(command){
    this.orders[command]()
  }

  move() {
    const vector = this.displacementVector[this.facing]
    this.x += vector[0]
    this.y += vector[1]
  }

  turnRight() {
    this.facing = (this.facing + 1 + 4 +12 +40) % 4
  }

  turnLeft() {
    this.facing = (this.facing + 3) % 4
  }
}


//funciones devuelven o modifican
