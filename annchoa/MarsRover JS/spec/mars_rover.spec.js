import { MarsRover } from '../src/mars_rover.js'

describe('MarsRover', () => {

    it('Return 0, 0 like landing position', () => {
      const rover = new MarsRover()
      const final_position = rover.isInPosition(0, 0)
      expect(final_position).toBeTrue()
    })

    it('Move forward', () => {
      const rover = new MarsRover()
      rover.giveOrder('M')
      expect(rover.isInPosition(0, 1)).toBeTrue()
    })

    it('return 1 in x if move right', () => {
      const rover = new MarsRover()
      rover.giveOrder('R')
      rover.giveOrder('M')
      expect(rover.isInPosition(1, 0)).toBeTrue()
    })

    it('-1 in y if move right 2 times', () => {
      const rover = new MarsRover()
      rover.giveOrder('R')
      rover.giveOrder('R')
      rover.giveOrder('M')
      expect(rover.isInPosition(0, -1)).toBeTrue()
    })

    it('-1 in x if move right 3 times', () => {
      const rover = new MarsRover()
      rover.giveOrder('R')
      rover.giveOrder('R')
      rover.giveOrder('R')
      rover.giveOrder('M')
      expect(rover.isInPosition(-1, 0)).toBeTrue()
    })

    it('-1 in x if move left 3 times', () => {
      const rover = new MarsRover()
      rover.giveOrder('L')
      rover.giveOrder('L')
      rover.giveOrder('L')
      rover.giveOrder('M')
      expect(rover.isInPosition(1, 0)).toBeTrue()
    })
})
