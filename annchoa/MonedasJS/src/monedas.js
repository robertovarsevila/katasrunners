export class Monedas {
  constructor (quantity) {
    this.quantity = this.extractQuantity(quantity)
    this.coins = {
      '2€': 200,
      '1€': 100,
      '50cent': 50,
      '20cent': 20,
      '10cent': 10,
      '5cent': 5,
      '2cent': 2,
      '1cent': 1,
    }
  }

  getChange() {
    if (this.quantity === 0) {
      return '0 monedas'
    }

    const change = this.calculateChange()
    const total_coins = this.totalCoins(change)
    return this.buildSentence(total_coins)
  }

  extractQuantity(quantity) {
    if (quantity.includes('€')) {
      const position = quantity.indexOf('€')
      return parseFloat(quantity.substring(0, position)) * 100
    }
    if (quantity.includes('cent')) {
      const position = quantity.indexOf('cent')
      return parseInt(quantity.substring(0, position))
    }
  }

  buildSentence(total_coins) {
    let coin_type = Object.keys(total_coins)
    let sentence = ''

    const lastCoin = coin_type.pop()

    for (let coin of coin_type){
      const number = total_coins[coin]
      sentence = this.addToSentence(sentence, ', ', number, coin)
    }

    const number = total_coins[lastCoin]
    sentence = this.addToSentence(sentence, ' y ', number, lastCoin)

    return sentence
  }

  addToSentence(sentence, glue, amount, coin) {
    if (sentence != ''){
      sentence += glue
    }
    sentence += this.buildSingleCoinSentence(amount, coin)
    return sentence
  }

  buildSingleCoinSentence(amount, coin) {
    let plural = ''
    if (amount !== 1){
      plural = 's'
    }
    return `${amount} moneda${plural} de ${coin}`
  }

  calculateChange() {
    const change = []

    for (const coin in this.coins){
      const value = this.coins[coin]

      while (this.quantity >= value){
        this.quantity -= value
        change.push(coin)
      }
    }
    return change
  }

  countCoins(change, cent) {
      const coin_repeat = change.filter(coin => coin == cent)
      return coin_repeat.length
  }

  totalCoins(change) {
    const filter_coins = {}
    for (const coin in this.coins){
      const value = this.coins[coin]
      const count = this.countCoins(change, coin)
      if (count >= 1){
        filter_coins[coin] = count
      }
    }
    return filter_coins
  }
}

//funciones devuelven o modifican
//for in
//for of
