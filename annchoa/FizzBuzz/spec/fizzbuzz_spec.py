from mamba import *
from expects import *
from fizzbuzz import *

FIZZ = 'Fizz'
BUZZ = 'Buzz'
FIZZBUZZ = 'FizzBuzz'
PUNSET = 'Punset'
rules = [
  FizzBuzzRule(),
  FizzRule(),
  BuzzRule(),
  EverythingElseStringRule()
]

with description('FizzBuzz') as self:
  with it('multiple of 3 return Fizz'):
    fizz_buzz = FizzBuzz(rules)
    is_multiple = fizz_buzz.fizz_buzz(18)
    expect(is_multiple).to(equal(FIZZ))

  with it('multiple of 5 return buzz') as self:
    fizz_buzz = FizzBuzz(rules)
    is_multiple = fizz_buzz.fizz_buzz(25)
    expect(is_multiple).to(equal(BUZZ))

  with it('multiple of 3 and 5 return fizzbuzz') as self:
   fizz_buzz = FizzBuzz(rules)
   is_multiple = fizz_buzz.fizz_buzz(15)
   expect(is_multiple).to(equal(FIZZBUZZ))

  with it('number not multiple of 3 and 5 return str') as self:
    fizz_buzz = FizzBuzz(rules)
    is_multiple = fizz_buzz.fizz_buzz(17)
    expect(is_multiple).to(equal('17'))

  with it('number has a 3 return fizz') as self:
    fizz_buzz = FizzBuzz(rules)
    is_multiple = fizz_buzz.fizz_buzz(13)
    expect(is_multiple).to(equal(FIZZ))

  with it('number has a 5 return buzz') as self:
    fizz_buzz = FizzBuzz(rules)
    is_multiple = fizz_buzz.fizz_buzz(52)
    expect(is_multiple).to(equal(BUZZ))

  with it('number has a 3 and 5 as str return fizzbuzz') as self:
    fizz_buzz = FizzBuzz(rules)
    is_multiple = fizz_buzz.fizz_buzz(53)
    expect(is_multiple).to(equal(FIZZBUZZ))
