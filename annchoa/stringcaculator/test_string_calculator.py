import unittest
from stringcalculator import StringCalculator

class TestStringCalculator(unittest.TestCase):

    def test_get_0_for_non_arguments(self):
        string_calculator = StringCalculator()
        calculated = string_calculator.string_argument("")
        self.assertEqual(calculated, 0)

    def test_get_0_for_one_argument(self):
        string_calculator = StringCalculator()
        calculated = string_calculator.string_argument("1")
        self.assertEqual(calculated, 1)

    def test_get_0_for_two_arguments(self):
        string_calculator = StringCalculator()
        calculated = string_calculator.string_argument("1, 4")
        self.assertEqual(calculated, 5)

    def test_handle_an_unknown_amount_of_numbers(self):
        string_calculator = StringCalculator()
        calculated = string_calculator.string_argument("1,6,7,8,10")
        self.assertEqual(calculated, 32)

    def test_handle_new_lines_between_numbers(self):
        string_calculator = StringCalculator()
        calculated = string_calculator.string_argument("1\n2,3")
        self.assertEqual(calculated, 6)


#--!
if __name__=='__main__':
    unittest.main()
