import unittest
from mars_rover import MarsRover

class TestMarsRover(unittest.TestCase):

    def test_knows_its_position(self):
        rover = MarsRover()
        is_in_position = rover.are_you_in_position(0, 0)
        self.assertTrue(is_in_position)

    def test_moves_forward(self):
        rover = MarsRover()
        rover.move()
        rover.move()
        is_in_position = rover.are_you_in_position(2, 0)
        self.assertTrue(is_in_position)

    def test_turns_left(self):
        rover = MarsRover()
        rover.turn_left()
        rover.turn_left()
        rover.move()
        is_in_position = rover.are_you_in_position(-1, 0)
        self.assertTrue(is_in_position)

    def test_can_turn_left_360(self):
        rover = MarsRover()
        rover.turn_left()
        rover.turn_left()
        rover.turn_left()
        rover.turn_left()
        rover.move()
        is_in_position = rover.are_you_in_position(1, 0)
        self.assertTrue(is_in_position)

    def test_can_turn_right_360(self):
        rover = MarsRover()
        rover.turn_right()
        rover.turn_right()
        rover.turn_right()
        rover.turn_right()
        rover.move()
        is_in_position = rover.are_you_in_position(1, 0)
        self.assertTrue(is_in_position)
#--!
if __name__ == '__main__':
    unittest.main()
