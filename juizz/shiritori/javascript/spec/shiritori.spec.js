import {Shiritori} from '../src/shiritori.js'

describe('shiritori', () => {
    it('begins with a word', () => {
        // Arrange
        const shiritori = new Shiritori()
        const word = 'Hola'

        // Act
        const result = shiritori.turn(word)

        // Assert
        expect(result).toBe('Ok')
    })
    it('game over when a word does not start with previouse word last letter', () => {
        // Arrange
        const shiritori = new Shiritori()
        const word = 'Hola'
        const next_word = 'Paco'
        shiritori.turn(word)

        // Act
        const result = shiritori.turn(next_word)

        // Assert
        expect(result).toBe('Game over')
    })
    it('is corrent when a word starts with previouse word last letter', () => {
        // Arrange
        const shiritori = new Shiritori()
        const word = 'Hola'
        const next_word = 'abaricoque'
        shiritori.turn(word)

        // Act
        const result = shiritori.turn(next_word)

        // Assert
        expect(result).toBe('Ok')
    })
    it('receive words until game over', () => {
        // Arrange
        const shiritori = new Shiritori()
        shiritori.turn('Hola')
        shiritori.turn('abaricoque')
        shiritori.turn('empanao')
        // Act
        const result = shiritori.turn('jarana')

        // Assert
        expect(result).toBe('Game over')
    })
    it('game over when repeat a word', () => {
        // Arrange
        const shiritori = new Shiritori()
        shiritori.turn('Hola')
        shiritori.turn('abaricoque')
        shiritori.turn('empanada')
        // Act
        const result = shiritori.turn('abaricoque')

        // Assert
        expect(result).toBe('Game over')
    })

    it('keep playing when response before equal 10 seconds', () =>{
        const shiritori = new Shiritori()
        shiritori.turn('Hola', 0)
        // Act
        const result = shiritori.turn('abaricoque', 13)
        // Assert
        expect(result).toBe('Game over')
        
    })
})