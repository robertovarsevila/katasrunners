from mamba import *
from expects import *
from fizzbuzz import fizzbuzz, FizzBuzzList

with description('FizzBuzz') as self:
  with it('returns a fizzbuzzlist'):
    fizzbuzz = FizzBuzzList()

    expect(type(fizzbuzz)).to(equal(FizzBuzzList))

  with it('returns a list with 100 elements'):
    fizzbuzz = FizzBuzzList()
    
    result = fizzbuzz

    expect(len(result)).to(equal(100))

  with it('defines dinamically the range of numbers in the list'):
      #arrange
      size_list = 200 
      fizzbuzz = FizzBuzzList(size_list)
      #act
      result = fizzbuzz
      #assert
      expect(len(result)).to(equal(200))
      
  with it('first list element is 1'):
    fizzbuzz = FizzBuzzList()

    expect(fizzbuzz[0]).to(equal('1'))

  with it('returns Fizz instead of divisible numbres by three'):
    fizzbuzz = FizzBuzzList()

    expect(fizzbuzz[2]).to(equal("Fizz"))
    expect(fizzbuzz[5]).to(equal("Fizz"))

  with it('returns Fizz instead of divisible numbres by five'):
      fizzbuzz = FizzBuzzList()

      expect(fizzbuzz[4]).to(equal("Buzz"))
      expect(fizzbuzz[9]).to(equal("Buzz"))

  with it('returns FizzBuzz instead of divisible numbres by five and three '):
      fizzbuzz = FizzBuzzList()

      expect(fizzbuzz[14]).to(equal("FizzBuzz"))
      expect(fizzbuzz[29]).to(equal("FizzBuzz"))

  with it('returns Fizz when number contains a 3 '):
      fizzbuzz = FizzBuzzList()

      expect(fizzbuzz[12]).to(equal("Fizz"))
  
  with it('returns Buzz when number contains a 5 '):
      fizzbuzz = FizzBuzzList()

      expect(fizzbuzz[51]).to(equal("Buzz"))

