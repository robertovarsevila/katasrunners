import { romannumeralconverter } from '../src/romanconverter.js'

describe('converter', () => {
    it('should return II', () => {
        // Arrange
        const expected_value = 'II'

        // Act
        const result = romannumeralconverter(2)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return III', () => {
        // Arrange
        const expected_value = 'III'

        // Act
        const result = romannumeralconverter(3)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return IV', () => {
        // Arrange
        const expected_value = 'IV'

        // Act
        const result = romannumeralconverter(4)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return V', () => {
        // Arrange
        const expected_value = 'V'

        // Act
        const result = romannumeralconverter(5)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return IX', () => {
        // Arrange
        const expected_value = 'IX'

        // Act
        const result = romannumeralconverter(9)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return XII', () => {
        // Arrange
        const expected_value = 'XII'

        // Act
        const result = romannumeralconverter(12)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return XVI', () => {
        // Arrange
        const expected_value = 'XVI'

        // Act
        const result = romannumeralconverter(16)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return XXIX', () => {
        // Arrange
        const expected_value = 'XXIX'

        // Act
        const result = romannumeralconverter(29)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return XLIV', () => {
        // Arrange
        const expected_value = 'XLIV'

        // Act
        const result = romannumeralconverter(44)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return XLV', () => {
        // Arrange
        const expected_value = 'XLV'

        // Act
        const result = romannumeralconverter(45)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return LXVIII', () => {
        // Arrange
        const expected_value = 'LXVIII'

        // Act
        const result = romannumeralconverter(68)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return LXXXIII', () => {
        // Arrange
        const expected_value = 'LXXXIII'

        // Act
        const result = romannumeralconverter(83)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return XCVII', () => {
        // Arrange
        const expected_value = 'XCVII'

        // Act
        const result = romannumeralconverter(97)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return XCIX', () => {
        // Arrange
        const expected_value = 'XCIX'

        // Act
        const result = romannumeralconverter(99)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return CD', () => {
        // Arrange
        const expected_value = 'CD'

        // Act
        const result = romannumeralconverter(400)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return D', () => {
        // Arrange
        const expected_value = 'D'

        // Act
        const result = romannumeralconverter(500)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return DI', () => {
        // Arrange
        const expected_value = 'DI'

        // Act
        const result = romannumeralconverter(501)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return DCXLIX', () => {
        // Arrange
        const expected_value = 'DCXLIX'

        // Act
        const result = romannumeralconverter(649)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return DCCXCVIII', () => {
        // Arrange
        const expected_value = 'DCCXCVIII'

        // Act
        const result = romannumeralconverter(798)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return DCCCXCI', () => {
        // Arrange
        const expected_value = 'DCCCXCI'

        // Act
        const result = romannumeralconverter(891)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return M', () => {
        // Arrange
        const expected_value = 'M'

        // Act
        const result = romannumeralconverter(1000)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return MIV', () => {
        // Arrange
        const expected_value = 'MIV'

        // Act
        const result = romannumeralconverter(1004)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return MVI', () => {
        // Arrange
        const expected_value = 'MVI'

        // Act
        const result = romannumeralconverter(1006)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return MXXIII', () => {
        // Arrange
        const expected_value = 'MXXIII'

        // Act
        const result = romannumeralconverter(1023)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return MMXIV', () => {
        // Arrange
        const expected_value = 'MMXIV'

        // Act
        const result = romannumeralconverter(2014)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return MMMCMXCIX', () => {
        // Arrange
        const expected_value = 'MMMCMXCIX'

        // Act
        const result = romannumeralconverter(3999)

        // Assert
        expect(result).toBe(expected_value)
    })

})
