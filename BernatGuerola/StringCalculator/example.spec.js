import { StringCalculator } from '../src/StringCalculator.js'

describe('String Calculator test', () => {
    it('returns 0 for a empty string', () => {
        // Arrange
        const num = ''
        const result = 0

        // Act
        const expected_value = StringCalculator(num)

        // Assert
        expect(result).toBe(expected_value)
    })
})

    it('returns the number for a single number string', () => {
        // Arrange
        const num = 6
        const result = 6

        // Act
        const expected_value = StringCalculator(num)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('returns the sum of the numbers of the given string', () => {
        // Arrange
        const num = ('123,4')
        const result = 127

        // Act
        const expected_value = StringCalculator(num)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('returns an array of numbers for a mixed number and symbols string', () => {
        // Arrange
        const num = "1\n2,3"
        const result = 6

        // Act
        const expected_value = StringCalculator(num)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('returns only the sum of numbers smaller 1000', () => {
        // Arrange
        const num = "1,1003,3"
        const result = 4

        // Act
        const expected_value = StringCalculator(num)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should not allow negative numbers', () => {
        // Arrange
        const num = "1,1003,-3"
        const result = 'negatives not allowed: -3'

        // Act
        const expected_value = StringCalculator(num)

        // Assert
        expect(result).toBe(expected_value)
    })