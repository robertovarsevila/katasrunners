package Fizzbuzz;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.jupiter.api.Test;

class PrincipalTest {
	
	@Test //torna la cadena tal qual del 1 al 100 davant de qualsevol numero
	void testTornarCadena() {
		String resultado=Principal.crearLlista(4).get(4);
		String esperado= "4";
		assertEquals(esperado, resultado);
	}
	
	@Test //als numeros divisibles sols per 3 escriu "Fizz"
	void testTornarFizz() {
		String resultado=Principal.crearLlista(9).get(9);
		String esperado="Fizz";
		assertEquals(esperado,resultado);
	}
	
	@Test //als numeros divisibles sols per 5 escriu "Fizz"
	void testTornarBuzz() {
		String resultado=Principal.crearLlista(5).get(5);
		String esperado="Buzz";
		assertEquals(esperado,resultado);
	}
	
	@Test //als numeros divisibles per 5 i per 3 escriu "Fizz"
	void testTornarFizzBuzz() {
		String resultado=Principal.crearLlista(15).get(15);
		String esperado="FizzBuzz";
		assertEquals(esperado,resultado);
	}
	
}
