export function stringCalculator(string_del_test) {
    const separador_split = /,|\n/;
    const nuevo_separador_split = /\/\/.\n/;
    const regex_numeros_negativos = /-?\d+/g;

    function suma_numeros_del_array(array_a_sumar) {
        var array_numeros_pequenos = selecciona_numeros_menores(array_a_sumar);

        var suma_numeros_del_array = Array.from(array_numeros_pequenos).reduce((acumulador, item_del_array) => {
            return acumulador + item_del_array;
            })
        return suma_numeros_del_array;
    }

    // Hace una criba y deshecha los números mayores de 1000
    function selecciona_numeros_menores(array_a_seleccionar){
        var string_sin_numeros_grandes = array_a_seleccionar.filter((item_del_array) => item_del_array <= 1000);
        return Array.from(string_sin_numeros_grandes);
    }
    if (string_del_test.length == 0) {
        return 0;
    }
    else if (string_del_test.split(",").some((item_del_array) => item_del_array === "\n") === true) {
        return "No admite '\n' sin un número que lo acompañe";
    }
    else if (string_del_test.match(regex_numeros_negativos).map(Number).some((item_del_array) => item_del_array < 0) === true){
        console.log("\nnegatives not allowed: " + string_del_test.match(/-\d+/g).map(Number));
        return "negatives not allowed";
    }
    else if (string_del_test.match(nuevo_separador_split) != null){
        var extraidos_solo_numeros = string_del_test.match(/\d+/g);
        var array_string_con_numeros = extraidos_solo_numeros.map(Number);
        return suma_numeros_del_array(array_string_con_numeros);
    }
    else {
        var array_con_numeros = string_del_test.split(separador_split).map(Number);
        return suma_numeros_del_array(array_con_numeros);
    }
}
