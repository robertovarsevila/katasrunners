require_relative 'cash_register'
require 'test/unit'

class TestCashRegister < Test::Unit::TestCase
  EMPTY_DRAWER = []
  ONE_PENNY_DRAWER = [['PENNY', 0.01]]
  TWO_PENNIES_DRAWER = [['PENNY', 0.02]]
  THREE_PENNIES_DRAWER = [['PENNY', 0.03]]
  TWO_COINS_IN_DRAWER = [['PENNY', 0.23], ['NICKEL', 0.15]]
  ALL_COINS_DRAWER = [['PENNY', 1.01], ['NICKEL', 2.05], ['DIME', 3.1], ['QUARTER', 4.25], ['ONE', 90], ['FIVE', 55], ['TEN', 20], ['TWENTY', 60], ['ONE_HUNDRED', 100]]

  def test_a_returns_insufficient_funds_when_there_is_not_enough_change_in_drawer
    some_price = 0.01
    some_cash = 0.03
    expected_result = {'status' => 'insufficient_funds', 'change' => []}

    actual_result = check_cash_register(some_price, some_cash, ONE_PENNY_DRAWER)

    assert_equal(expected_result, actual_result)
  end

  def test_b_returns_closed_drawer_when_cash_in_drawer_equals_change_due
    some_price = 0.01
    some_cash = 0.02
    expected_result = { 'status' => 'closed', 'change' => ONE_PENNY_DRAWER }

    actual_result = check_cash_register(some_price, some_cash, ONE_PENNY_DRAWER)

    assert_equal(expected_result, actual_result)
  end

  def test_c_returns_empty_change_due_when_exact_cash_is_payed
    some_price = 0.01
    some_cash = 0.01
    expected_result = { 'status' => 'open', 'change' => EMPTY_DRAWER }

    actual_result = check_cash_register(some_price, some_cash, ONE_PENNY_DRAWER)

    assert_equal(expected_result, actual_result)
  end

  def test_d_returns_a_single_penny_when_the_change_is_one_penny
    some_price = 0.01
    some_cash = 0.02
    expected_result = { 'status' => 'open', 'change' => ONE_PENNY_DRAWER }

    actual_result = check_cash_register(some_price, some_cash, TWO_PENNIES_DRAWER)

    assert_equal(expected_result, actual_result)
  end

  def test_e_return_the_change_when_drawer_has_single_coin_type
    some_price = 0.01
    some_cash = 0.03
    expected_result = { 'status' => 'open', 'change' => TWO_PENNIES_DRAWER }

    actual_result = check_cash_register(some_price, some_cash, THREE_PENNIES_DRAWER)

    assert_equal(expected_result, actual_result)
  end

  def test_f_handles_returning_change_when_having_two_coins_in_drawer_and_returning_a_single_coin_type
    some_price = 0.01
    some_cash = 0.03
    expected_result = { 'status' => 'open', 'change' => TWO_PENNIES_DRAWER }

    actual_result = check_cash_register(some_price, some_cash, TWO_COINS_IN_DRAWER)

    assert_equal(expected_result, actual_result)
  end

  def test_g_handles_returning_change_when_having_two_coins_in_drawer_and_returning_coins_of_two_types
    some_price = 0.01
    some_cash = 0.13
    expected_result = { 'status' => 'open', 'change' => [["NICKEL", 0.10], ["PENNY", 0.02]] }

    actual_result = check_cash_register(some_price, some_cash, TWO_COINS_IN_DRAWER)

    assert_equal(expected_result, actual_result)
  end

  def test_h_handles_returning_the_right_change_independently_of_the_types_of_coins
    some_price = 3.26
    some_cash = 100
    expected_change = expected_change = [['TWENTY', 60], ['TEN', 20], ['FIVE', 15], ['ONE', 1], ['QUARTER', 0.5], ['DIME', 0.2], ['PENNY', 0.04]]
    expected_result = { 'status' => 'open', 'change' => ALL_COINS_DRAWER }

    actual_result = check_cash_register(some_price, some_cash, ALL_COINS_DRAWER)

    assert_equal(expected_result, actual_result)
  end
end
