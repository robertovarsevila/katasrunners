# Caja registradora

Diseña una función de caja registradora `checkCashRegister()` que acepte el precio de la compra como primer argumento (`price`), el pago como segundo argumento (`cash`) y el dinero en caja (`cid` de cash-in-drawer) como tercer argumento.

`cid` es un array bidimensional listando todas las monedas disponibles.

La función `checkCashRegister()` siempre deberá devolver un objeto con las claves `status` y `change`.

Devuelve `{ status: "INSUFFICIENT_FUNDS", change: [] }` si el dinero en la caja es menor al dinero que hay que devolver o si no puedes devolver el cambio exacto.

Devuelve `{ status: "CLOSED", change: [...] }` si el dinero en la caja como valor de la clave `change` si el dinero en la caja es igual al cambio a devolver.

Si no, devuelve `{ status: "OPEN", change: [...] }` con el cambio a devolver en monedas y billetes, ordenado poniendo las monedas con el valor mas alto primero como valor para la clave `change`.

## Instalación y uso

### Javascript

#### Ejecutando los tests

`npm test`

#### Instalación

- Primero instala NodeJS. Puedes encontrar los detalles [en su web](https://nodejs.org/en/). Esto instalará `Node` y `npm`.
- Instala las dependencias utilizando `npm`: `npm install`

### Python

#### Ejecutando los tests

`python3 -m unittest test_example.py`

#### Instalación

- Instala Python 3. Puedes encontrar los detalles [en su web](https://www.python.org/downloads/).
