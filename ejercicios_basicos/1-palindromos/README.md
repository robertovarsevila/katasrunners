# Palíndromos

Devuelve `true` si la cadena de texto dada es un palíndromo. Si no, devuelve `false`.

Un _palíndromo_ es una palabra o frase que se deletrea de la misma manera tanto hacia alante como hacia atrás, ignorando la puntuación, los espacios y las mayúsculas/minúsculas.

## Nota

Tendrás que eliminar **todos los caracteres no alfanuméricos** (puntuación, espacios y símbolos) y convertir todo a lo mismo (mayúsculas o minúsculas) para poder comprobar si son palíndromos.

Pasaremos cadenas de texto con varios formatos, como `racecar`, `RaceCar` o `raceCAR` entre otros.

También pasaremos cadenas de texto con caracteres especiales como `2A3*3a2`, `2A3 3a2` o `2_A3*3#A2`.

## Instalación y uso

### Javascript

#### Ejecutando los tests

`npm test`

#### Instalación

- Primero instala NodeJS. Puedes encontrar los detalles [en su web](https://nodejs.org/en/). Esto instalará `Node` y `npm`.
- Instala las dependencias utilizando `npm`: `npm install`

### Python

#### Ejecutando los tests

`python3 -m unittest test_example.py`

#### Instalación

- Instala Python 3. Puedes encontrar los detalles [en su web](https://www.python.org/downloads/).

### Ruby

#### Ejecutando los tests

`ruby test_palindrome.rb`

#### Instalación

- Instala el intérprete de Ruby. Puedes encontrar los detalles [en su web](https://www.ruby-lang.org/es/)
