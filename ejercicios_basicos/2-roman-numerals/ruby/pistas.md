# Roman Numerals Converter

## Explicación del problema

Escribirás un programa que convierta de números árabes a números romanos.

### Enlaces relevantes

- (Roman numerals)[https://www.mathsisfun.com/roman-numerals.html]

## Pistas

### Pista 1

Crear dos arrays, uno con los numeros romanos y otra con sus equivalencias árabes te será de mucha ayuda.

## Pista 2

No puedes tener más de tres números romanos iguales consecutivos.
