import { winningElections } from '../src/winningElections.js'

describe('Example', () => {
    it('should return true', () => {
        // Arrange
        const expected_value = true

        // Act
        const result = winningElections('a string')

        // Assert
        expect(result).toBe(expected_value)
    })
})
