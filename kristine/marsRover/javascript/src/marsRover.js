const N = 0
const E = 1
const S = 2
const W = 3
const OUT_OF_RANGE = 4

export class MarsRover {
    constructor(positionX, positionY, direction, commands = ""){
        this.positionX = positionX
        this.positionY = positionY
        this.direction = direction
        this.commands = commands
        this.executeCommands()
    }
    
    executeCommands(){
        for(let i = 0; i < this.commands.length; i++){
            if(this.commands[i] === "M"){
                this.move()
            }
            if(this.commands[i] === "R"){              
                this.rotateRight()
            }
            if(this.commands[i] === "L"){              
                this.rotateLeft()
            }
        }
    }

    move(){
        switch(this.direction){
            case N: 
                this.positionY++
                break
            case E:
                this.positionX++
                break
            case S:
                this.positionY--
                break
            case W:
                this.positionX--
                break
        }
    }

    rotateRight(){
        this.direction = ++this.direction % 4
    }

    rotateLeft(){
        this.direction--
        if(this.direction < 0){
            this.direction = 3
        }
    }

    getPosition(){  
        return [this.positionX, this.positionY, this.direction]
    }

    getCommands(){
        return this.commands
    }
}
