
export class Greeting {

    greet(names) {
        if(Array.isArray(names)) {
            let splited_names = []
            let reversed_names = names.slice().reverse()
            for(let name of reversed_names) {
             let splited_name = name.split(', ')
             splited_names = splited_name.concat(splited_names)
            }
           return this.arrayGreet(splited_names)
        }
         return this.stringGreet(names)
    }
    
    arrayGreet(names) {
        if(names.length >= 3) {
            let greetingValue = "Hello"
            for( let oneName of names) {
                if(oneName == names[names.length-1]){
                    greetingValue +=  ' and ' + oneName + '.'
                }else if(!this.isUpperCase(oneName)){
                    greetingValue += ', ' + oneName
                }
            }
            for(let oneName of names) {
                if(this.isUpperCase(oneName)) {
                    greetingValue += ` AND HELLO ${oneName}!`
                }
            }
                return greetingValue
        }
        return `Hello, ${names[0]} and ${names[1]}.`
    }


    stringGreet(name) {
        if ( name == null) {
            return "Hello, my friend"
        }

        if(this.isUpperCase(name)) {
            return `HELLO ${name}!`
        }
        return `hello, ${name}`
    }
    
    isUpperCase(name) {
        return name == name.toUpperCase()
    }

}
