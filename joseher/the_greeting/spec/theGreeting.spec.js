import { theGreeting } from '../src/theGreeting.js'

describe('The Greeting', () => {
    it('should return a greeting for the input name', () => {
        // Arrange
        const expected_value = "Hello, Bob."

        // Act
        const result = theGreeting("Bob")

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return a stand-in when the input is null', () => {
        // Arrange
        const expected_value = "Hello, my friend."

        // Act
        const result = theGreeting()

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return the greeting in uppercase when the input is in uppercase', () => {
        // Arrange
        const expected_value = "HELLO, BOB!"

        // Act
        const result = theGreeting("BOB")

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should handle two names of input', () => {
        // Arrange
        const expected_value = "Hello, Bob and Jane."

        // Act
        const result = theGreeting(["Bob", "Jane"])

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should handle an arbitrary names of input', () => {
        // Arrange
        const expected_value = "Hello, Bob, Jane and Jill."

        // Act
        const result = theGreeting(["Bob", "Jane", "Jill"])

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should handle an arbitrary names of input and  a "shout"', () => {
        // Arrange
        const expected_value = "Hello, Bob, Jane and Jill. AND HELLO BRIAN!"

        // Act
        const result = theGreeting(["Bob", "Jane", "BRIAN", "Jill"])

        // Assert
        expect(result).toBe(expected_value)
    })
})
