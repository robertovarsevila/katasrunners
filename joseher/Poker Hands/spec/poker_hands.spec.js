import { PokerHands } from '../src/poker_hands.js'

describe('Poker Hands', () => {
     it('should return the higher card', () => {
        // Arrange
        const black_hand = new PokerHands('2H 4D 7S QC 3H')
        const white_hand = '4C JH 6S 2S 8D'

        const expected_value = `Black player wins - With high card: Q`

        // Act
        const result = black_hand.compare(white_hand)

        // Assert
        expect(result).toBe(expected_value)
    }) 
    it('should return the pairs', () => {
        // Arrange
        const black_hand = new PokerHands('2H KD 5S QC 5H')
        const white_hand = "4C 2H 6S 2S AD"
        const expected_value = `Black player wins - With pair: 5`

        // Act
        const result = black_hand.compare(white_hand)

        // Assert
        expect(result).toBe(expected_value)
        
    }) 
    it('should return the highest poker hand', () => {
        // Arrange
        const black_hand = new PokerHands('2H 3H 4H 5H 6H')
        const white_hand = "4C 4H 6S 4S AD"
        const expected_value = `Black player wins - With straight flush: 2,3,4,5,6`

        // Act
        const result = black_hand.compare(white_hand)

        // Assert
        expect(result).toBe(expected_value)
        
    })
})
