export class PokerHands {
    constructor (hand) {
        this.black_poker_hand = stringToArray(hand)
        this.card_values = ["2", "3", "4", "5", "6", "7", "8", "9", "T", "J", "Q", "K", "A"]
        this.card_suit = ["C", "D", "H", "S"]
        this.poker_hands = ["high card", "pair", "two pairs", "three of a kind", "straight", "flush", "full house", "four of a kind", "straight flush"]
    }

    compare(other_hand){
        let white_poker_hand = stringToArray(other_hand)
        
        let black_hand_values = this.getValues(this.black_poker_hand)
        let black_hand = black_hand_values[0]
        let black_values = black_hand_values[1]
        
        let white_hand_values = this.getValues(white_poker_hand)
        let white_hand = white_hand_values[0]
        let white_values = white_hand_values[1]
     
        if (this.poker_hands.indexOf(black_hand) > this.poker_hands.indexOf(white_hand)) {
            return `Black player wins - With ${black_hand}: ${black_values}`
        }

        if (this.poker_hands.indexOf(black_hand) < this.poker_hands.indexOf(white_hand)) {
            return `White player wins - With ${white_hand}: ${white_values}`
        }

        if (this.card_values.indexOf(black_values) > this.card_values.indexOf(white_values)) {
            return `Black player wins - With ${black_hand}: ${black_values}`
        }

        return `White player wins - With ${white_hand}: ${white_values}`
    }

    getValues(poker_hand) {
        const positions = poker_hand.filter(card => this.card_values.includes(card))

        positions.sort((a, b) => {
            return this.card_values.indexOf(a) - this.card_values.indexOf(b);
        });

        const suits = poker_hand.filter(card => this.card_suit.includes(card))

        return this.whichPokerHand(positions, suits)  
    }

    whichPokerHand(value, suit) {
        const equals = value.filter(item => value.includes(item, value.indexOf(item) + 1))

        if (this.card_values.indexOf(value[0]) + 4 == this.card_values.indexOf(value[4]) && suit.every(i => i == suit[0]) == true) {
            return ["straight flush", value]
        }
        if (equals.length == 4 & equals[0] == equals[3]) {
            return ["four of a kind", equals[0]]
        }
        if (equals.length == 5 & equals[0] != equals[4]) {
            return ["full house", equals[0, 4]]
        }
        if (suit.every(i => i == suit[0]) == true) {
            return ["flush", suit[0]]
        }
        if (this.card_values.indexOf(value[0]) + 4 == this.card_values.indexOf(value[4])) {
            return ["straight", value]
        }

        const items = ['high card','pair','three of a kind','two pair']
        if (equals.length == 0) {
            return [items[0], value[4]]
        }
        return [items[equals.length -1], equals.pop()]
    }
}

function stringToArray(string_to_convert) {
    let remove_spaces = string_to_convert.toString().replace(/[\W_]/g, "")
    return remove_spaces.split("")
}