import { GameOfLife } from '../src/gameoflife.js'

//Genera tablas de forma aleatoria, así que me es imposible aplicar test de esta forma. Pero funciona. 

describe('Game Of life', () => {
    it('should return an arbitrary grid of cells', () => {
        // Arrange
        const game_of_life = new GameOfLife (5,5);
        const expected_value = [
            [1,1,0,0,0],
            [1,1,0,0,0],
            [0,0,0,0,0],
            [0,0,0,0,0],
            [0,0,0,0,0],

        ]
        game_of_life.grid = [
            [0,1,0,0,0],
            [1,1,0,0,0],
            [0,0,0,0,0],
            [0,0,0,0,0],
            [0,0,0,0,0],
        ]
        console.table(game_of_life.grid);
        game_of_life.life_or_death();
        console.table(game_of_life.grid);

        // Act
        const result = game_of_life.grid

        // Assert
        expect(result).toEqual(expected_value)
    })
    

    xit(' Any live cell with fewer than two live neighbours dies', () => {
        // Arrange
        const Game_of_Life = new GameOfLife (10,10);
        const expected_value = true

        // Act
        const result = Game_of_Life.get_grid(10, 10)

        // Assert
        expect(result).toBe(expected_value)
    })
    
})
