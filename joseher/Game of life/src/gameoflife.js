export class GameOfLife {
    constructor (x, y) {
        this.rows = x
        this.columns = y
        this.get_grid()
    }

    get_grid() { // Genera un grid del tamaño que queramos y lo rellena aleatoriamente de ceros y unos.
        this.grid = new Array();
        for (let i = 0; i < this.rows; i++){
            this.grid[i] = []
            for (let j = 0; j < this.columns; j++){
                this.grid[i][j] = Math.floor(Math.random() * 2);
            }
        }
    }

    life_or_death(){ // Aplica las normas del game of life en relación a los vecinos que tiene una posición
        let generation = new Array()
        for (let i = 0; i < this.rows; i++){
            generation[i] = new Array()
            for (let j = 0; j < this.columns; j++){

                let status = this.grid[i][j];
               
                let neighbours = this.count_neighbours(i, j)
                
                if (status == 0 & neighbours == 3) {
                    generation[i][j] = 1
                } else if (status == 1 && (neighbours < 2 || neighbours > 3)){
                    generation[i][j] = 0
                } else {
                    generation[i][j] = status;
                }
            }
        }
        this.grid = generation
    }    
    
    count_neighbours(row, col){ // Suma los unos que tiene alrededor una posición
        let total = 0; 

        for (let i = -1; i < 2; i++) {
            for (let j = -1; j < 2; j++) {
                if (!(i === 0 && j === 0)) {
                    total += this.get_cell_status(row + i, col + j)
                }
            }
        }
        return total
    }

    get_cell_status (x, y){ // Si una posición es una esquina o un lateral devuelve un 0 en vez de undefined
        if (this.grid[x] !== undefined && this.grid[x][y] !== undefined ) {
            return this.grid[x][y]
        }
        return 0
    }
}