import { MarsRover } from '../src/marsRover.js'
const NORTH = 0
const EAST = 1
const SOUTH = 2
const WEST = 3
const DIMENSION = 5

describe('MarsRover', () => {

    describe('Yellow belt', () => {

        it('Mars Rover returns its position', () => {
            // Arrange
            const marsRover = new MarsRover(5, 5, NORTH)

            let expectedPosition = [5, 5, NORTH]

            // Act
            let getPosition = marsRover.getPosition()

            // Assert
            expect(getPosition).toEqual(expectedPosition)
        })

        it('Mars Rover receives commands', () => {
            // Arrange
            const marsRover = new MarsRover(1, 2, NORTH, 'MR')
            let expectedCommand = 'MR'

            // Act
            let commands = marsRover.getCommands()

            // Assert
            expect(commands).toEqual(expectedCommand)
        })

        it('Mars Rover receives string commands', () => {
            // Arrange
            const marsRover = new MarsRover(1, 2, NORTH, 'M')
            let expectedCommand = 'M'

            // Act
            let commands = marsRover.getCommands()

            // Assert
            expect(commands).toEqual(expectedCommand)
            expect(typeof commands).toEqual('string')
        })
    })

    describe('Green belt', () => {

        it('Mars Rover receives M command and moves forward', () => {
            // Arrange
            const marsRover = new MarsRover(1, 2, NORTH, 'MMM')
            let expectedPosition = [1, 5, NORTH]

            // Act
            let position = marsRover.getPosition()

            // Assert
            expect(position).toEqual(expectedPosition)
        })

        fit('When the rover recives the command "R" it turns right', () => {
            // Arrange
            const marsRover = new MarsRover(1, 2, NORTH, "R")
            let expectedPosition = [1, 2, EAST]

            // Act
            let position = marsRover.getPosition()

            // Assert
            expect(position).toEqual(expectedPosition)
        })

        it('When the rover recives the command "L" it turns left', () => {
            // Arrange
            const marsRover = new MarsRover(1, 2, NORTH, "L")
            let expectedPosition = [1, 2, WEST]

            // Act
            let position = marsRover.getPosition()

            // Assert
            expect(position).toEqual(expectedPosition)
        })

        it('When the rover recives the multiple commands it does what it has to do', () => {
            // Arrange
            const marsRover = new MarsRover(0, 0, NORTH, "LMMRMML")
            let expectedPosition = [4, 2, WEST]

            // Act
            let position = marsRover.getPosition()

            // Assert
            expect(position).toEqual(expectedPosition)
        })
    })

    describe('Red belt', () => {

        it('Rover knows world dimensions', () => {
            // Arrange
            const marsRover = new MarsRover(5, 0, NORTH, "RMRM")
            let expectedPosition = [0, 5, SOUTH]

            // Act
            let position = marsRover.getPosition()

            // Assert
            expect(position).toEqual(expectedPosition)
        })
    })

})