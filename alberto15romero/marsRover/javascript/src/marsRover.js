const NORTH = 0
const EAST = 1
const SOUTH = 2
const WEST = 3
const DIMENSION = 5

export class MarsRover {

    constructor(positionX, positionY, direction, commands = "") {

        this.dict = {
            'M': this.move,
            'R': this.rotateRight,
            'L': this.rotateLeft
        }

        this.positionX = positionX
        this.positionY = positionY
        this.direction = direction
        this.commands = commands
        this.executeCommands()
    }

    executeCommands() {
        for (let i = 0; i < this.commands.length; i++) {
            this.dict[this.commands[i]]
        }
    }

    move() {
        switch (this.direction) {
            case NORTH:
                this.positionY = ++this.positionY % (DIMENSION + 1)
                break
            case EAST:
                this.positionX = ++this.positionX % (DIMENSION + 1)
                break
            case SOUTH:
                this.positionY--
                    if (this.positionY < 0) {
                        this.positionY = DIMENSION
                    }

                break
            case WEST:
                this.positionX--
                    if (this.positionX < 0) {
                        this.positionX = DIMENSION
                    }
                break
        }
    }

    rotateRight() {
        this.direction = ++this.direction % 4
    }

    rotateLeft() {
        this.direction--
            if (this.direction < 0) {
                this.direction = 3
            }
    }

    getPosition() {
        return [this.positionX, this.positionY, this.direction]
    }

    getCommands() {
        return this.commands
    }
}